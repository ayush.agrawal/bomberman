from glob import *
import random
from termcolor import colored

enmy=[['E','E','E','E'],['E','E','E','E']]
class enemy():

	def __init__(self,x,y,move,count):
		self.x=x
		self.y=y
		self.move=move
		self.count=count

	def remove_it(self):
		for i in range(2):
			for j in range(4):
				arr[self.x+i][self.y+j]=' '

	def place_enemy(self):
		for i in range(2):
			for j in range(4):
				arr[self.x+i][self.y+j]=colored(enmy[i][j],'red')

	def move_left(self):
		if arr[self.x][self.y-4]==' ' and arr[self.x+1][self.y-4]==' ':
			self.remove_it()
			self.y-=4
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=colored(enmy[i][j],'red')
			return 1
		return 0

	def move_right(self):
		if arr[self.x][self.y+7]==' ' and arr[self.x+1][self.y+7]==' ':
			self.remove_it()
			self.y+=4
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=colored(enmy[i][j],'red')
			return 1
		return 0

	def move_up(self):
		if arr[self.x-2][self.y]==' ' and arr[self.x-2][self.y+1]==' ' and arr[self.x-2][self.y+2]==' ' and arr[self.x-2][self.y+3]==' ':
			self.remove_it()
			self.x-=2
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=colored(enmy[i][j],'red')
			return 1
		return 0

	def move_down(self):
		if arr[self.x+3][self.y]==' ' and arr[self.x+3][self.y+1]==' ' and arr[self.x+3][self.y+2]==' ' and arr[self.x+3][self.y+3]==' ':
			self.remove_it()
			self.x+=2
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=colored(enmy[i][j],'red')
			return 1
		return 0



	def move_enemy(self,level):
		mthds={1: self.move_right,2: self.move_left,3: self.move_up,4: self.move_down}
		temp=random.randint(1,4)
		if not(mthds[self.move]()) or self.count==14-level:
			mthds[temp]()
			self.move=temp
			self.count=0
		else:
			self.count+=1
		#while mthds[temp]()!=1:
		#	temp=random.randint(1,4)
		# if self.move_right():
		# 	return 
		# if self.move_left():
		# 	return
		# if self.move_down():
		# 	return 
		# if self.move_up():
		# 	return