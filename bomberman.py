from glob import *
from person import person
from termcolor import colored


man=[[colored('[','blue'),colored('^','blue'),colored('^','blue'),colored(']','blue')],[' ',colored(']','blue'),colored('[','blue'),' ']]

class Bomberman:

	def __init__(self,x,y):
		self.x=x
		self.y=y
		for i in range(2):
			for j in range(4):
				arr[2+i][4+j]=man[i][j]

	def remove_it(self):
		for i in range(2):
			for j in range(4):
				arr[self.x+i][self.y+j]=' '


	def move_left(self):
		if arr[self.x][self.y-4]==' ' and arr[self.x+1][self.y-4]==' ':
			self.remove_it()
			self.y-=4
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=man[i][j]

	def move_right(self):
		if arr[self.x][self.y+7]==' ' and arr[self.x+1][self.y+7]==' ':
			self.remove_it()
			self.y+=4
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=man[i][j]

	def move_up(self):
		if arr[self.x-2][self.y]==' ' and arr[self.x-2][self.y+1]==' ' and arr[self.x-2][self.y+2]==' ' and arr[self.x-2][self.y+3]==' ':
			self.remove_it()
			self.x-=2
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=man[i][j]

	def move_down(self):
		if arr[self.x+3][self.y]==' ' and arr[self.x+3][self.y+1]==' ' and arr[self.x+3][self.y+2]==' ' and arr[self.x+3][self.y+3]==' ':
			self.remove_it()
			self.x+=2
			for i in range(2):
				for j in range(4):
					arr[self.x+i][self.y+j]=man[i][j]

	def is_enemy_nearby(self):
		if arr[self.x][self.y-4]==colored('E','red') or arr[self.x][self.y+4]==colored('E','red') or arr[self.x-2][self.y]==colored('E','red') or arr[self.x+2][self.y]==colored('E','red'):
			return 1
		else:
			return 0
